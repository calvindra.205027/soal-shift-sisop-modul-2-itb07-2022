#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(void){
  
pid_t dir_darat, dir_air, dir_extract, pisah_darat, pisah_air, dir_pisah, dir_pisah2, dir_pisah_coba, dir_pisah2_coba, hapus_sisa, hapus_bird, hapus_bird2, buat_file, pindah_file;
int status;
char file[100];
        
  dir_darat = fork();
  if(dir_darat < 0){
    exit(EXIT_FAILURE);
  }

  if(dir_darat == 0){ // membuat folder darat
    char *argv[] = {"mkdir", "-p", "darat", NULL};
    execv("/bin/mkdir", argv);
  } else{
    while((wait(&status)) > 0);
    dir_air = fork();

    if(dir_air < 0){
      exit(EXIT_FAILURE);
    }

    if(dir_air == 0){ // membuat folder air
      sleep(3); //diberi sleep 3 detik setelah pembuatan folder darat
      char *argv[] = {"mkdir", "-p", "air", NULL};
      execv("/bin/mkdir", argv);
    } else{
      while((wait(&status)) > 0);
      dir_extract = fork();

      if(dir_extract < 0){
        exit(EXIT_FAILURE);
      }

      if(dir_extract == 0){ // mengunzip atau mengekstrak file animal.zip menjadi folder
        char *argv[] = {"unzip", "-q", "animal.zip", NULL};
        execv("/usr/bin/unzip", argv);
      } 
        
    else{ 
        sleep(3);
        while((wait(&status)) > 0);
        dir_pisah = fork();

        if(dir_pisah < 0){
          exit(EXIT_FAILURE);
        }

        if(dir_pisah == 0){ //agar file hewan air di folder animal bisa masuk di folder air
        char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*air.jpg", "-exec", "mv", "-t", "/home/kali/modul2/air", "{}", "+", (char *) NULL};
        execv("/usr/bin/find",arg);
          
        }
         else{
          while((wait(&status)) > 0);
          dir_pisah2 = fork();

          if(dir_pisah2 < 0){
            exit(EXIT_FAILURE);
          }

          if(dir_pisah2 == 0){ //agar file hewan darat di folder animal bisa masuk di folder darat
          char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*darat.jpg", "-exec", "mv", "-t", "/home/kali/modul2/darat", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
          }
          }
    
} 
       while((wait(&status)) > 0);
        dir_pisah_coba = fork();

        if(dir_pisah_coba < 0){
          exit(EXIT_FAILURE);
        }

        if(dir_pisah_coba == 0){ // melakukan pemisahan lagi untuk file  sisa dari  pada pemisahan pertama
        char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*fish.jpg", "-exec", "mv", "-t", "/home/kali/modul2/air", "{}", "+", (char *) NULL};
        execv("/usr/bin/find",arg);
        }
         else{
          while((wait(&status)) > 0);
          dir_pisah2_coba = fork();

          if(dir_pisah2_coba < 0){
          exit(EXIT_FAILURE);
          }

          if(dir_pisah2_coba == 0){
          char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*bird.jpg", "-exec", "mv", "-t", "/home/kali/modul2/darat", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
          }}

          while((wait(&status)) > 0);
          hapus_sisa = fork();

          if(hapus_sisa < 0){
          exit(EXIT_FAILURE);
        }

        if(hapus_sisa == 0){ // untuk menghapus sisa gambar yang tersisa pada folder animal, dan file itu adalah frog
          char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*frog.jpg", "-exec", "rm", "-r", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
        }
         else{
          while((wait(&status)) > 0);
          hapus_bird = fork();

          if(hapus_bird < 0){
            exit(EXIT_FAILURE);
          }

          if(hapus_bird == 0){ // menghapus semua file burung pada folder darat
          char *arg[]={"find", "/home/kali/modul2/darat", "-type", "f", "-name", "*bird.jpg", "-exec", "rm", "-r",  "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
          }}

           while((wait(&status)) > 0);
          hapus_bird2 = fork();

          if(hapus_bird2 < 0){
          exit(EXIT_FAILURE);
          }

          if(hapus_bird2 == 0){
          char *arg[]={"find", "/home/kali/modul2/darat", "-type", "f", "-name", "*bird_darat.jpg", "-exec", "rm", "-r", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
          }


        sprintf(file, "list.txt"); // membuat list.txt
        buat_file = fork();
        if (buat_file == 0) {
        char *argv[] = {"touch", file, NULL};
        execv("/bin/touch", argv);
    }
        while((wait(&status)) > 0);
        pindah_file = fork();

        if(pindah_file < 0){
          exit(EXIT_FAILURE);
        }

        if(pindah_file == 0){ // file list txt yang awalnya di folder modul 2, kemudian dipindahkan ke dalam folder air
        char *arg[]={"find", "/home/kali/modul2/", "-type", "f", "-name", "*list.txt", "-exec", "mv", "-t", "/home/kali/modul2/air", "{}", "+", (char *) NULL};
        execv("/usr/bin/find",arg);
          
        }
       /*
        FILE *fp;
        fp = fopen (file, "w+");
        fprintf(fp, "%s ", "/home/kali/modul2/air");
        fclose(fp); */
      /*
      DIR *d;
    struct dirent *dir;
    d = opendir("/home/kali/modul2/air/");
    if (d != NULL) {
      while ((dir = readdir(d)) != NULL) {
        if(!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..")){}
        if(strstr(dir->d_name, "txt")){}
        else if(strstr(dir->d_name, "jpg")){
            FILE *air = fopen("/home/kali/modul2/air/list.txt", "a");
            fprintf(air, "%s\n", dir->d_name);
            fclose(air);
          }
    
      }
      closedir(d);
    } */




  }}}

  

      