# soal-shift-sisop-modul-2-ITB07-2022

## Laporan Resmi Modul 2 Praktikum Sistem Operasi 2022
---
### Kelompok ITB07:
Calvindra Laksmono Kumoro - 5027201020  
Afrida Rohmatin Nuriyah - 5027201037  
Brilianti Puspita Sari - 5027201070  

---
# Soal 1
**Analisa Soal**  
Inti dari soal ini yaitu diminta untuk membuat simulasi sistem gacha seperti yang ada pada game pada umumnya  
## 1A  
**Analisa Soal**  
Untuk program gacha ini, diawali dengan mendownload database characters dan weapons yang telah disediakan, kemudian program akan mengekstrak kedua file tersebut. Setelah itu, akan dibuat folder `gacha_gacha` yang nantinya akan dijadikan folder tempat penyimpanan hasil gacha.  
**Penyelesaian**  
Dikarenakan database characters dan weapons yang akan digunakan untuk gacha harus di download terlebih dahulu maka dibuat kode sebagai berikut:
```c
pid_t child_a, child_b;
child_a = fork();

  if (child_a == 0)
  {
    char *download[2][6] = {
      {"wget", "-q", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O", "dbCharacters", NULL},
      {"wget", "-q", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O", "dbItems", NULL}};

    for (int i = 0; i < 2; i++)
    {
      if (fork() == 0)
        continue;
        sleep(5);
        execv("/bin/wget", download[i]);
    }
```
Pertama, membuat child process dengan **fork()**, kemudian di dalam child process dibuat for loop untuk mengeksekusi `download[i]` yang akan melakukan download database sesuai dengan link yang telah disediakan dan menggunakan `execv("/bin/wget")`. Setelah mendownload kedua database, maka program akan melakukan ekstrak pada file tersebut dengan menggunakan kode:
```c
 else
      {
        while (wait(&mark) > 0)
          ;
        child_b = fork();
        int mark2;

        if (child_b == 0)
        {
          char *ekstrak[2][4] = {
              {"unzip", "-q", "dbCharacters", NULL},
              {"unzip", "-q", "dbItems", NULL}};

          for (int i = 0; i < 2; i++)
          {
            if (fork() == 0)
              continue;
            sleep(5);
            execv("/bin/unzip", ekstrak[i]);
          }
        }
```
Kode untuk melakukan ekstrak dilakukan pada parent process dengan cara membuat child process kedua dengan **fork()**. Kemudian, di dalam child process kedua terdapat kode untuk melakukan ekstrak dengan unzip. Pada for loop dilakukan eksekusi `ekstrak[i]` dengan menggunakan `exevc("/bin/unzip")`.  
Selain itu, pada soal 1A untuk membuat folder `gacha_gacha` sebagai working directory. Source codenya sebagai berikut:
```c
        char *folder[] = {"mkdir", "gacha_gacha", NULL};
        execv("/bin/mkdir", folder);
```
Untuk membuat folder dapat menggunakan `execv("/bin/mkdir")` yang akan megkesekusi argumen `folder`.
### Output soal 1A
![image](./img/output_1a.JPG)  

## 1B  
**Analisa Soal**  
Pada 1B, diminta untuk setiap kali gacha dilakukan, characters dan weapons bergantian diambil dari database dengan ketentuan:  
1. Jika jumlah gacha bernilai genap akan dilakukan gacha weapons
2. Jika jumlah gacha bernilai ganjil akan dilakukan gacha characters
3. Setiap kali jumlah gacha mod 10 akan dibuat file (.txt) baru yang akan dijadikan tempat hasil gacha selanjutnya
4. Setiap kali jumlah gacha mod 90 akan dibuat folder baru yang akan dijadikan tempat penyimpanan file (.txt) hasil gacha selanjutnya  

Sehingga, nantinya dalam satu folder terdapat 9 file (.txt) yang di dalam file tersebut terdapat 10 hasil gacha.

**Penyelesaian**  
Untuk memulai proses gacha, dibuat kode sebagai berikut pada parent process:
```c
  else
        {
          while (wait(&mark2) > 0)
            ;
          startGacha(0, primogems);
        }
```
Fungsi startGacha:
```c
void startGacha(int n, int gems)
{
  int jumlah = 0;
  int perfile = 0;
  char sekarang[20];

  while (gems > 160)
  {
    if (n % 2 == 0) //jika jumlah gacha genap
    {
      if (n % 10 == 0 && n % 90 == 0)
      {
        //membuat folder baru dan file (.txt)
      }
      else if (n % 10 == 0)
      {
       //membuat file (.txt)
      }

      //melakukan gacha untuk weapons
    else if (n % 2 == 1)
    {
      //melakukan gacha untuk characters
    }
    n++;
    gems = gems - 160;
    sleep(1);
  }
}

```
Fungsi di atas digunakan untuk melakukan gacha sesuai dengan ketentuan, `int jumlah` untuk menghitung jumlah setiap 90 yang nantinya akan digunakan untuk folder. `int perfile` untuk menghitung jumlah setiap 10 yang nantinya akan digunakan untuk file (.txt). Setelah, masuk ke while loop dengan kriteria yang sesuai maka `n`(jumlah gacha satuan) akan diincrement dan `gems` akan dikurang dengan 160 (berkaitan dengan soal 1C selanjutnya). Untuk masing-masing bagian kode di atas, akan dijelaskan pada soal 1C.
## 1C
**Analisa Soal**  
Ketentuan format nama file dan folder:
1. File (.txt) :  `{Hh:Mm:Ss}_gacha_{jumlah-gacha}`, misal 04:44:12_gacha_120.txt
2. Folder : `total_gacha_{jumlah-gacha}`  

**Penyelesaian**  
Lanjutan dari soal 1B
- Jika jumlah gacha genap, mod 10, dan mod 90
```c
  printf("Gacha dimulai..\n");
        jumlah = jumlah + 90;
        createFolder(jumlah);
        time_t currenttime;
        struct tm *hhmmss;
        time(&currenttime);
        hhmmss = localtime(&currenttime);
        sprintf(sekarang, "%d:%d:%d", hhmmss->tm_hour, hhmmss->tm_min, hhmmss->tm_sec);
        perfile = perfile + 10;
```
Maka program akan membuat folder dengan function `createFolder(jumlah)`. Kemudian, akan di ambil currenttime untuk penamaan file. Untuk mengambil currenttime dapat menggunakan `snprintf` yang nantinya akan disimpan di variabel `sekarang`.
- Fungsi createFolder()
```c
void createFolder(int jumlah)
{
  int mark = 0;
  if (fork() == 0)
  {
    char temp[256];
    snprintf(temp, sizeof temp, "/home/afridarn/SisOp/modul2/soal1/gacha_gacha/total_gacha_%d", jumlah);
    char *newfolder[] = {"mkdir", temp, NULL};
    execv("/bin/mkdir", newfolder);
  }

  while (wait(&mark) > 0)
    ;
}
```
Fungsi createFolder menggunakan parameter `jumlah` yang nantinya akan bertambah 90 setiap folder berhasil dibuat. Untuk pembuatan folder baru dapat menggunakan `execv("/bin/mkdir")` yang akan mengeksekusi argumen `newFolder` serta foldernya akan memiliki nama `total_gacha_jumlah` yang diambil menggunakan `snprintf`.
- Jika jumlah gacha genap dan mod 10
```c
else if (n % 10 == 0)
      {
        time_t currenttime;
        struct tm *hhmmss;
        time(&currenttime);
        hhmmss = localtime(&currenttime);
        sprintf(sekarang, "%d:%d:%d", hhmmss->tm_hour, hhmmss->tm_min, hhmmss->tm_sec);
        perfile += 10;
      }
```
Kode di atas akan mengambil current time menggunakan `snprintf` yang disimpan di variabel `sekarang`. Current time ini akan digunakan untuk penamaan file (.txt).
- Jika jumlah gacha genap
```c
  gachaWeapons(jumlah, perfile, sekarang, n, gems);
```
Untuk melakukan gacha pada item weapon, kami membuat function `gachaWeapons()`
- Function gachaWeapons()
```c
void gachaWeapons(int jumlah, int perfile, const char *waktu, int n, int gems)
{
  int mark = 0;
  if (fork() == 0)
  {
    FILE *d;
    char buffer[2500];
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    char temp[256];
    snprintf(temp, sizeof temp, "/home/afridarn/SisOp/modul2/soal1/weapons/%s", weaponsDirandom());
    d = fopen(temp, "r");
    fread(buffer, 2500, 1, d);
    fclose(d);
```
Inti dari fungsi ini yaitu untuk mengambil nilai properti name dan rarity dari random file yang terpilih. Pertama, membuat 3 struct json yang akan digunakan untuk mengambil nilai properti. Kemudian, menggunakan `snprintf` untuk menyimpan nama file yang akan diambil propertinya dan menyimpan nama file pada `temp`. Pada snprintf, terdapat `%s` yang akan diisi dari hasil function `weaponsDirandom`. Fungsi ini akan dijelaskan di bawah. Setelah itu, file tersebut akan dibuka dan dibaca menggunakan `fopen` dan disimpan di `buffer` lalu ditutup menggunakan `fclose`.
```c

    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    char res[50] = "_weapons_";
    strcat(res, json_object_get_string(name));
    strcat(res, "_");
    strcat(res, json_object_get_string(rarity));
    strcat(res, "_");

    char temp2[256];
    char temp3[256];
    snprintf(temp2, sizeof temp2, "%d%s%d", n, res, gems);
    snprintf(temp3, sizeof temp3, "/home/afridarn/SisOp/modul2/soal1/gacha_gacha/total_gacha_%d/%s_gacha_%d.txt", jumlah, waktu, perfile);
    FILE *out = fopen(temp3, "a");
    fprintf(out, "%s\n", temp2);
    fclose(out);
  }

```
Kemudian, dilakukan pengambilan properti name dan rarity dengan cara parse json dan dibuat file menggunakan `snprintf` dengan nama sesuai ketentuan `waktu_gacha_perfile`.
- Function weaponsDirandom()
```c
const char *weaponsDirandom()
{
  int mark = 0;
  if (fork() == 0)
  {
    srand(time(NULL));
    int min = 0;
    int max = 129;
    int arrNum = rand() % (max + 1 - min) + min;

    DIR *dir = opendir("/home/afridarn/SisOp/modul2/soal1/weapons/");
    int p = 0;
    char *filesArr[130];

    struct dirent *entity;
    entity = readdir(dir);
    while (entity != NULL)
    {
      if (!strcmp(entity->d_name, ".") || !strcmp(entity->d_name, ".."))
      {
      }
      else
      {
        filesArr[p] = (char *)malloc(strlen(entity->d_name) + 1);
        strcpy(filesArr[p], entity->d_name);
        p++;
      }
      entity = readdir(dir);
    }

    closedir(dir);

    return filesArr[arrNum];
  }

  while (wait(&mark) > 0)
    ;
}
```
Untuk melakukan pengambilan file database secara acak, dapat menggunakan `rand` dan `array`. Untuk mengambil angka acak, digunakan `srand(time(NULL))` yang akan mengambil nilai acak antara 0-129 setiap kali fungsi dipanggil. Kemudian, akan dilakukan file listing yang akan disimpan pada array. Lalu, fungsi akan mengembalikan `array[i]` dengan i hasil random dan array merupakan nama file.
- Jika jumlah gacha ganjil  

Untuk jumlah gacha ganjil (characters) prosesnya sama seperti gacha genap (weapons) hanya saja directory pengambilan database yang berbeda.
### Output Soal 1C  
- Folder di dalam folder gacha_gacha
![image](./img/output_1b_1.JPG)  
- File (.txt) yang terdapat dalam salah satu folder
![image](./img/output_1b_2.JPG)  
### 1D  
**Analisa Soal**  
Untuk melakukan satu kali gacha dibutuhkan suatu primogems sebanyak 16 gems dan untuk jumlah awal primogems didefine sebanyak `79000 gems`. Setiap kali gacha dilakukan, ada 2 properti yang diambil dari database yaitu, `name` dan `rarity`.  
Ketentuan output hasil gacha yang disimpan di dalam file txt:  
` {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}`  
Contoh: 157_characters_5_Albedo_53880  
**Penyelesaian**  
Untuk soal 1D, penyelesaian terdapat pada function `gachaWeapons` yang telah dijabarkan pada 1C pada bagian:
```c
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    char res[50] = "_weapons_";
    strcat(res, json_object_get_string(name));
    strcat(res, "_");
    strcat(res, json_object_get_string(rarity));
    strcat(res, "_");

    char temp2[256];
    char temp3[256];
    snprintf(temp2, sizeof temp2, "%d%s%d", n, res, gems);
```
Kode di atas akan melakukan parse json dan mengambil nilai `name` dan `rarity`. Kemudian, terdapat variabel `res` yang akan menyimpan hasil parse sesuai ketentuan soal. Lalu, terdapat variabel `temp2` yang akan menampung satu hasil gacha secara full sesuai format soal dengan menggunakan `snprintf`. Untuk melakukan gacha characters, penyelesainnya sama seperti gacha pada weapons, akan tetapi menggunakan function `gachaCharacters()`.
### Output Soal 1D
- File txt pertama
![image](./img/output_1d.JPG)   
- File txt terakhir
![image](./img/output_1d_2.JPG)  
### 1E
**Analisa Soal**  
Gacha akan dimulai pada `30 Maret pukul 04.44` dan setelah 3 jam semua isi yang ada pada folder `gacha_gacha` akan dizip dengan nama `not_safe_for_wibu` dan dipassword `satuduatiga`. Setelah itu, semua folder akan dihapus dan hanya menyisakan file (.zip).  
**Penyelesaian**  
Untuk melakukan program gacha pada waktu yang telah ditentukan dapat menggunakan `daemon`.
- Template daemon process yang ada pada modul
```c
int main()
{
  pid_t pid, sid;

  pid = fork();

  if (pid < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (pid > 0)
  {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0)
  {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/afridarn/SisOp/modul2/soal1/")) < 0)
  {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
  time_t currenttime = time(0);
  int stop = 1;

  while (1)
  {
    //tulis program kalian di sini

    sleep(30);
  }
}
```
Dalam while (1) di atas, kami menambahkan program yang akan dijalankan. Awalnya, membuat percabangan menggunakan if-else untuk membedakan program yang akan dijalankan pada 30 Maret 2022 04.44 dan untuk 3 jam setelahnya. Kode dapat ditulis sebagai berikut:
```c
int stop = 1;

  while (1)
  {
    currenttime = time(0);

    // epochtime dari 30 Maret 2022 04.44 -> 1648590240
    if (currenttime >= 1648590240 && stop == 1)
    {
      // Kode untuk penyelesaian soal a, b, c, d
      stop++;
    }
     else if (currenttime >= 1648601040)
    { 
      // Kode untuk penyelesaian setelah 3 jam
    }
  }
```
Untuk membedakan kedua bagian program, kami menggunakan format waktu epochtime. Epochtime dari 30 Maret 2022 04.44 adalah `1648590240` dan untuk 30 Maret 2022 07.44 adalah `1648601040`.
- Kode penyelesaian setelah 3 jam
```c
pid_t child_lagi;
      int mark = 0;
      child_lagi = fork();
      if (child_lagi == 0)
      {
        char *zipHasil[7] = {"zip", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
        execv("/bin/zip", zipHasil);
      }
      else
      {
        while ((wait(&mark)) > 0)
          ;
        char *selesai[4] = {"rm", "-v", "!(not_safe_for_wibu.zip)", NULL};
        execv("/bin/rm", selesai);
        exit(1);
      }
```
Di sini, kami membuat child process dengan **fork()**. Kemudian, pada child processnya akan melakukan zip folder `gacha_gacha` yang dieksekusi menggunakan `execv("/bin/zip")`. Kemudian, pada parentnya akan dilakukan penghapusan semua file kecuali file zip hasil gacha yaitu `not_safe_for_wibu.zip`.
### Output Soal 1E
- Cek daemon process apakah berjalan
![image](./img/output_1e_1.JPG)  
- Hasil zip setelah 3 jam
![image](./img/output_1e_2.JPG)
![image](./img/output_1e_3.JPG)    

### Kendala yang dihadapi
Untuk penamaan file txt tidak dapat berjarak 1 second karena pada saat dicoba satu detik maka hasil random akan menghasilkan kesamaan item sehingga setiap file akan berisi dengan hasil gacha yang sama. Selain itu, kendala lain yang dihadapi yaitu pada awalnya daemon process tidak dapat berjalan, tetapi hal itu sudah teratasi karena ada sedikit kesalahan dari kami yaitu lupa tidak mengubah directory ke working directory.

---
## Soal 2
## Exe. 2a
mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan. 

[Drakor Zip](https://drive.google.com/drive/folders/1_xdZG_oSfTaeuRD63eS-M2Ra-WG9vPDz)

### Analisa Soal
Pada soal ini kami diminta untuk extract file.zip dan memindahkan ke dalam folder yang bernama drakor. Dalam file.zip terdapat folder yang tidak penting maka kami diminta untuk menghapus folder yang tidak penting tersebut.

### Cara Pengerjaan
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2)

Disini saya menggunakan parent PID, dimana saya juga menggunakan child ID jadi setelah mengeksekusi parent PID proses akan langsung diserahkan ke child ID 
``` bash
void UnZip(pid_t child_id)
{
	child_id = fork();

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
```
pada proses unzip di sini saya menggunakan fork dan execv tersebut juga menggunakan execv.

char *argv[] = {"unzip", "/home/brillyant/shift2/drakor.zip", "-d", "/home/brillyant/shift2/drakor/", NULL
		};
		execv("/usr/bin/unzip", argv); 

di sini saya telah membaca file.zip lalu saya directory dengan menggunakan function unzip ke folder drakor dan menggunakan execv

**source by Modul 02**
![](Dokumentasi02/img11.png)

setelah unzip file.zip, pengerjaan selanjutnya adalah menghapus folder yang tidak penting dalam file tersebut.

![](Dokumentasi02/img14.png)

```bash 
char *argvrem[] = {"rm", "-rf", remove, NULL};
				    execv("/bin/rm", argvrem);
                    
                    

if (strcmp(d->d_name, ".") != 0 && strcmp(d->d_name, "..") != 0) {
		    if (d->d_type == 4) {
			    char remove[100] = "/home/brillyant/shift2/drakor/";
  ```    

sama seperti di awal saya menggunakan fork execv, dimana saya menggunakan function rm -rf untuk menghapus folder beserta isinya tanpa permission, untuk file yang dihapus saya sudah menginisialisasinya           

### Kendala
Pada awal pengerjaan folder drakor belum bisa muncul setelah di run, terdapat kesalahan penulisan code parent PID dan child ID.

Pada proses penghapusan folder yang tidak penting, tidak ditemukan kendala

![](Dokumentasi02/img12.png)

## Exe. 2b

program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.

### Analisa Soal
di dalam file.zip terdapat beberapa poster drama korea

![](Dokumentasi02/img13.png)

kami diminta untuk membuat program yang bisa membagi poster tersebut sesuai dengan kategori di dalam file yang memiliki nama sesuai kategori drakor tersebut.

### Cara Pengerjaan
Pertama, kami lakukan adalah memberi inisialisasi pada variabel yang akan kami butuhkan. 

- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2)

- [Modul Sisop Github](https://github.com/Lambangaw/soal-shift-sisop-modul-2-IT02-2021)

pada penamaan setiap poster yang terdapat pada folder drakor itu dipisahkan menggunakan **;** di antara satu variabel dengan satu variabel yang lain.

disini kami menggunakan token dengan memberikan command bahwa token akan membaca setiap satu titik koma.

sprintf(Judul, "%s", d->d_name);
				token = strtok(Judul,";");


token = strtok(NULL, ";_.");
				snprintf(kategori, sizeof kategori, "%s", token);

token akan terus berubah sampai dengan inisialisasi token terakhir, karena yang diminta yaitu membuat folder berdasarkan genre/kategori maka token terakhir akan membaca kategori drakor tersebut.   

char *argv[] = {"mkdir", "-p", AfterChg, NULL};
					execv("/bin/mkdir", argv);
                   
kami menggunakan untuk membuat folder dengan penamaan sesuai dengan kategori drakor.

### Kendala
Pada pengerjaan soal 2b terdapat kendala dimana beberapa poster ada yang masuk ke dalam folder yang tidak sesuai dengan nama poster drakor tersebut. Namun sudah berhasil, karena terdapat salah penulisan pada parent proses.

![](Dokumentasi02/img15.png)

## Exe. 2c

Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.

### Analisa Soal
kami diminta untuk menaruh setiap poster drakor tersebut ke dalam folder yang sesuai dengan kategori poster drakor tersebut. Pada soal juga diminta untuk mengubah nama poster menjadi /drakor/kategori/judul.png

### Cara Pengerjaan


- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2)

- [Modul Sisop Github](https://github.com/Lambangaw/soal-shift-sisop-modul-2-IT02-2021)

untuk me rename file yang akan masuk pada folder setiap kategori tersebut, kami membuat syntax berikut :
```
sprintf(rename, "drakor/%s/%s.png", kategori, Judul);

char *argvcopy[] = {"cp", BeforeChg, rename, NULL};
					execv("/bin/cp", argvcopy);
```
cp disini yaitu untuk meng copy semua file di drakor ke dalam folder, penyalinan tersebut di dasarkan dengan nama folder yang sesuai dengan genre/kategorinya.

### Kendala
beberapa poster ada yang terinput ke dalam kategori yang berbeda.

![](Dokumentasi02/img16.png)

## Exe. 2d
Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.

### Analisa Soal
terdapat suatu kasus dimana pada file poster tersebut memiliki 2 poster 
![](Dokumentasi02/img17.png)
di proses selanjutnya kita hanya menyalin poster tersebut ke dalam folder kategori, karena beberapa file poster ada yang digunakan untuk kasus seperti ini. pada file double poster ini memiliki kategori yang berbeda, tantangannya yaitu kami menempatkan poster dengan ketagori yang bebrbeda di dalam folder yang berbeda.

### Cara Pengerjaan
Cara inisialiasasi dan variabel masih sama seperti soal 2b dan 2c karena proses pengerjaannya memang seperti dari awal untuk poster kedua yang masih belum kami proses.

perbedaannya adalah, pada file penamaan poster itu diberi tanda pemisah dengan menggunakan **_** jadi syntax token yang akan kami butuhkan akan seperti berikut :

```
token=strtok(NULL, "_");
```

untuk token selanjutnya dan sampai proses rename sama seperti sub soal 2b dan 2c.

untuk proses memindahkan file sesuai dengan folder kategori, di sini kami menggunakan **agrmove** karena sudah tidak ada file poster yang akan digunakan setelah proses sub soal 2c ini berjalan.

{
						char *argvmove[] = {"mv", DualOld, rename_x, NULL};
						execv("/usr/bin/mv", argvmove);
					}

maka semua poster yang terdapat pada folder drakor akan pindah ke dalam sub folder drakor yaitu folder kategori.

### Kendala
tidak ditemukan kendala

![](Dokumentasi02/img18.png)
---
## Soal 3

# soal-shift-sisop-modul-2-ITB07-2022

ITB07

## Soal 3a
## Analisa soal 3a:
Pada nomor 3 pertama Conan diminta untuk membuat 2 directory darat dan setelah itu diberi jeda waktu 3 untuk membuat directory air.

## Cara pengerjaan 3a:
Pada soal ini diminta untuk membuat 2 directory pada “/home/[USER]/modul2/” dengan menggunakan  execv("/bin/mkdir", argv) dan menggunakan fork untuk membuat child proses pertama kemudian untuk membuat folder selanjutnya menggunakan fork lagi untuk membuat child proses kedua. Dan   sleep(3); digunakan untuk bisa memberi jangka waktu 3 detik setelah pembuatan directory pertama
```c
  dir_darat = fork();
  if(dir_darat < 0){
    exit(EXIT_FAILURE);
  }

  if(dir_darat == 0){ // membuat folder darat
    char *argv[] = {"mkdir", "-p", "darat", NULL};
    execv("/bin/mkdir", argv);
  } else{
    while((wait(&status)) > 0);
    dir_air = fork();

    if(dir_air < 0){
      exit(EXIT_FAILURE);
    }

    if(dir_air == 0){ // membuat folder air
      sleep(3); //diberi sleep 3 detik setelah pembuatan folder darat
      char *argv[] = {"mkdir", "-p", "air", NULL};
      execv("/bin/mkdir", argv);
    }
```

##  Hasil Run 3a
![user.txt](./img/3a.JPG)
![user.txt](./img/statdir.JPG)

## Kendala 3a: 
Untuk kendala pada nomor 3a belum ada

## Soal 3b
## Analisa Soal 3b:
Pada soal ini kita diminta untuk bisa mengextract animal.zip pada directory “/home/[USER]/modul2/”.


## Cara pengerjaan 1b:
Pada program ini membuat child proses ketiga untuk melakukan unzip dengan menggunakan method execv("/usr/bin/unzip", argv);

```c
else{
      while((wait(&status)) > 0);
      dir_extract = fork();

      if(dir_extract < 0){
        exit(EXIT_FAILURE);
      }

      if(dir_extract == 0){ // mengunzip atau mengekstrak file animal.zip menjadi folder
        char *argv[] = {"unzip", "-q", "animal.zip", NULL};
        execv("/usr/bin/unzip", argv);
      } 
```
## Hasil Run 3b:
![user.txt](./img/3b.JPG)

## Kendala 3b:
Untuk nomer 3b kami belum menemukan kendala

## Soal 3c
## Analisa Soal 3c:
Pada soal diminta untuk bisa memindahkan atau memisahkan isi folder animal berdasarkan jenis hewan yang ada yaitu hewan darat dan air pada masing-masing folder darat dan air. Kemudian rentang waktu pemindahan  file hewan berjenis darat dan air memiliki rentang waktu 3 detik. kemudian, setelah berhasil dipindahkan perlu untuk menghapus sisa hewan yang tersisa pada folder animal. 

## Cara Pengerjaan 3c:
Untuk bisa memisahkan kita perlu menggunakan pointer dari char *arg[] untuk bisa memisahkan ke folder masing-masing. Setelah itu untuk bisa menemukan hewan tersebut kita menggunakan find dengan syntax execv("/usr/bin/find",arg); , dan hewan akan dipisahkan juga dengan menggunakan mv yang terdapat pada isi syntax char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*(namayangingindipisahkan).jpg", "-exec", "mv", "-t", "/home/kali/modul2/air", "{}", "+", (char *) NULL};. Dan perlu juga menentukan letak lokasi awal file dan lokasi tujuan directory dimana nanti file akan dipindahkan.
```c
if(dir_pisah == 0){ //agar file hewan air di folder animal bisa masuk di folder air
        char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*air.jpg", "-exec", "mv", "-t", "/home/kali/modul2/air", "{}", "+", (char *) NULL};
        execv("/usr/bin/find",arg);
          
        }
         else{
          while((wait(&status)) > 0);
          dir_pisah2 = fork();

          if(dir_pisah2 < 0){
            exit(EXIT_FAILURE);
          }

          if(dir_pisah2 == 0){ //agar file hewan darat di folder animal bisa masuk di folder darat
          char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*darat.jpg", "-exec", "mv", "-t", "/home/kali/modul2/darat", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
          }
          }
```
Kemudian karena masih belum semua terhapus karena adanya sisa seperti format nama fish.jpg dan bird.jpg, kita perlu kembali mengulangi seperti algoritma pada di atas.
```c
 if(dir_pisah_coba == 0){ // melakukan pemisahan lagi untuk file  sisa dari  pada pemisahan pertama
        char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*fish.jpg", "-exec", "mv", "-t", "/home/kali/modul2/air", "{}", "+", (char *) NULL};
        execv("/usr/bin/find",arg);
        }
         else{
          while((wait(&status)) > 0);
          dir_pisah2_coba = fork();

          if(dir_pisah2_coba < 0){
          exit(EXIT_FAILURE);
          }

          if(dir_pisah2_coba == 0){
          char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*bird.jpg", "-exec", "mv", "-t", "/home/kali/modul2/darat", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
          }}
```
Setelah itu karena soal memberi perintah untuk menghapus sisa file yang tidak memiliki format nama_darat atau nama_air, kami tetap menggunakan cara di atas dengan mengganti menjadi *frog pada hewan yang dituju untuk dihapus, dan mengganti penggunaan mv menjadi rm untuk bisa bisa meremove file tersebut.
```c
 while((wait(&status)) > 0);
          hapus_sisa = fork();

          if(hapus_sisa < 0){
          exit(EXIT_FAILURE);
        }

        if(hapus_sisa == 0){ // untuk menghapus sisa gambar yang tersisa pada folder animal, dan file itu adalah frog
          char *arg[]={"find", "/home/kali/modul2/animal", "-type", "f", "-name", "*frog.jpg", "-exec", "rm", "-r", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
        }
```


## Hasil Run 3c:
![user.txt](./img/3cdarat.JPG)
![user.txt](./img/3cair.JPG)
![user.txt](./img/3canimal.JPG)


## Kendala 3c:
Kendala yang kami temui pada nomor ini yaitu sebelumnya kami sempat bingung untuk memindah hewan bird dan fish karena format nama yang berbeda dari yang dituju untuk dilakukan pemindahan atau pemisahan.

## Soal 3d 
## Analisa soal 3d:
Pada soal 1d ini conan diminta untuk menghapus semua file burung yang ada pada directory 

## Cara Pengerjaan 1d:
Program ini menggunakan syntax yang sama untuk menghapus sisa tadi, tetapi dengan mengganti *frog menjadi *bird dan *bird_darat agar tidak ada lagi file tersisa yang mengandung nama bird pada folder darat.
```c
else{
          while((wait(&status)) > 0);
          hapus_bird = fork();

          if(hapus_bird < 0){
            exit(EXIT_FAILURE);
          }

          if(hapus_bird == 0){ // menghapus semua file burung pada folder darat
          char *arg[]={"find", "/home/kali/modul2/darat", "-type", "f", "-name", "*bird.jpg", "-exec", "rm", "-r",  "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
          }}

           while((wait(&status)) > 0);
          hapus_bird2 = fork();

          if(hapus_bird2 < 0){
          exit(EXIT_FAILURE);
          }

          if(hapus_bird2 == 0){
          char *arg[]={"find", "/home/kali/modul2/darat", "-type", "f", "-name", "*bird_darat.jpg", "-exec", "rm", "-r", "{}", "+", (char *) NULL};
          execv("/usr/bin/find",arg);
          }
```

## Hasil run 3d:
![user.txt](./img/3d.JPG)
## Kendala 3d:
Untuk kendala nomer ini belum ada karena berhasil terpecahkan dari permasalahan soal 3d.

