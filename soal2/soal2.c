#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

//2A
void UnZip(pid_t child_id)
{
	child_id = fork();

	if (child_id < 0){
		exit(EXIT_FAILURE);
		}

	if (child_id == 0){
		char *argv[] = {"unzip", "/home/brillyant/shift2/drakor.zip", "-d", "/home/brillyant/shift2/drakor/", NULL
		};
		execv("/usr/bin/unzip", argv);
	}
	while (wait(NULL) != child_id);
}
void ListStr(char *basePath, pid_t child_id){
	char path[100];
	struct dirent *d;
	DIR *dirc = opendir(basePath);

	if (!dirc)
		return;
		
        while ((d = readdir(dirc)) != NULL) {
	    if (strcmp(d->d_name, ".") != 0 && strcmp(d->d_name, "..") != 0) {
		    if (d->d_type == 4) {
			    char remove[100] = "/home/brillyant/shift2/drakor/";
			    strcat(remove, d->d_name);

			    child_id = fork();

			    if (child_id < 0) {
				    exit(EXIT_FAILURE);
			    }

			    if (child_id == 0) {
				    char *argvrem[] = {"rm", "-rf", remove, NULL};
				    execv("/bin/rm", argvrem);
			    }
			    while (wait(NULL) != child_id);
		    }
			else {
				char AfterChg[1000];
				char BeforeChg[1000];
				char Judul[500];
				char kategori[100];
				char tahun[5];
				char *token;
				char rename[2000];

				sprintf(Judul, "%s", d->d_name);
				token = strtok(Judul,";");

				token = strtok(NULL, ";");
				snprintf(tahun, sizeof tahun, "%s", token);

				token = strtok(NULL, ";_.");
				snprintf(kategori, sizeof kategori, "%s", token);

				sprintf(AfterChg, "/home/brillyant/shift2/drakor/%s", token);
				sprintf(BeforeChg, "/home/brillyant/shift2/drakor/%s", d->d_name);

				child_id = fork();
//2B
				if (child_id < 0) {
					exit(EXIT_FAILURE);
				}

				if (child_id == 0) {				
					char *argv[] = {"mkdir", "-p", AfterChg, NULL};
					execv("/bin/mkdir", argv);					
				}

				while (wait(NULL) != child_id);

//2C
				sprintf(rename, "drakor/%s/%s.png", kategori, Judul);

				child_id = fork();
				if (child_id == 0) {
					char *argvcopy[] = {"cp", BeforeChg, rename, NULL};
					execv("/bin/cp", argvcopy);
				}
				while (wait(NULL) != child_id);
//2D
				char DualNew[1000]; char DualOld[1000]; char Judul_x[500]; char kategori_x[100]; char tahun_x[5]; char rename_x[2000];

				token=strtok(NULL, "_");
				sprintf(Judul_x, "%s", d->d_name);
				token = strtok(Judul_x,";");

				token = strtok(NULL, ";");
				snprintf(tahun_x, sizeof tahun_x, "%s", token);

				token = strtok(NULL, ";_.");
				snprintf(kategori_x, sizeof kategori_x, "%s", token);

				token = strtok(NULL, ";.");

				if(strlen(token) > 3) {
					sprintf(DualOld, "/home/brillyant/shift2/drakor/%s", d->d_name);

					sprintf(Judul_x, "%s", token);

					token = strtok(NULL,  ";_.");
					snprintf(tahun_x, sizeof tahun_x, "%s", token);

					token = strtok(NULL, "._");
					snprintf(kategori_x, sizeof kategori_x, "%s", token);

					sprintf(DualNew, "/home/brillyant/shift2/drakor/%s", token);

					child_id = fork();

					if (child_id < 0) {
						exit(EXIT_FAILURE);
					}

					if (child_id == 0) {				
						char *argv[] = {"mkdir", "-p", DualNew, NULL};
						execv("/bin/mkdir", argv);					
					}

					while (wait(NULL) != child_id);

					sprintf(rename_x, "drakor/%s/%s.png", kategori_x, Judul_x);

					child_id = fork();
					if (child_id == 0) {
						char *argvmove[] = {"mv", DualOld, rename_x, NULL};
						execv("/usr/bin/mv", argvmove);
					}
					while (wait(NULL) != child_id);
				}
			}
	    }
    }
	char *argvremove[] = {"find", "/home/brillyant/shift2/drakor/" ,"-maxdepth", "1", "-type", "f", "-exec", "rm", "{}", ";", NULL};
	execv("/usr/bin/find", argvremove);	
}

int main() {
	pid_t child_id;
	
	char path[100] = "/home/brillyant/shift2/drakor/";

	UnZip(child_id);
    ListStr(path, child_id);

	return 0;
}
