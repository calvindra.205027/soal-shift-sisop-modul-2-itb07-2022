#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <time.h>
#include <json-c/json.h>

const char *charactersDirandom()
{
  int mark = 0;
  if (fork() == 0)
  {
    srand(time(NULL));
    int min = 0;
    int max = 47;
    int arrNum = rand() % (max + 1 - min) + min;

    DIR *dir = opendir("/home/afridarn/SisOp/modul2/soal1/characters/");
    int p = 0;
    char *filesArr[48];

    struct dirent *entity;
    entity = readdir(dir);

    while (entity != NULL)
    {
      if (!strcmp(entity->d_name, ".") || !strcmp(entity->d_name, ".."))
      {
      }
      else
      {
        filesArr[p] = (char *)malloc(strlen(entity->d_name) + 1);
        strcpy(filesArr[p], entity->d_name);
        p++;
      }
      entity = readdir(dir);
    }

    closedir(dir);

    return filesArr[arrNum];
  }

  while (wait(&mark) > 0)
    ;
}

const char *weaponsDirandom()
{
  int mark = 0;
  if (fork() == 0)
  {
    srand(time(NULL));
    int min = 0;
    int max = 129;
    int arrNum = rand() % (max + 1 - min) + min;

    DIR *dir = opendir("/home/afridarn/SisOp/modul2/soal1/weapons/");
    int p = 0;
    char *filesArr[130];

    struct dirent *entity;
    entity = readdir(dir);
    while (entity != NULL)
    {
      if (!strcmp(entity->d_name, ".") || !strcmp(entity->d_name, ".."))
      {
      }
      else
      {
        filesArr[p] = (char *)malloc(strlen(entity->d_name) + 1);
        strcpy(filesArr[p], entity->d_name);
        p++;
      }
      entity = readdir(dir);
    }

    closedir(dir);

    return filesArr[arrNum];
  }

  while (wait(&mark) > 0)
    ;
}

void gachaCharacters(int jumlah, int perfile, const char *waktu, int n, int gems)
{
  int mark = 0;
  if (fork() == 0)
  {
    FILE *d;
    char buffer[2500];
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    char temp[256];
    snprintf(temp, sizeof temp, "/home/afridarn/SisOp/modul2/soal1/characters/%s", charactersDirandom());
    d = fopen(temp, "r");
    fread(buffer, 2500, 1, d);
    fclose(d);

    parsed_json = json_tokener_parse(buffer);

    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    char res[50] = "_characters_";
    strcat(res, json_object_get_string(name));
    strcat(res, "_");
    strcat(res, json_object_get_string(rarity));
    strcat(res, "_");

    char temp2[256];
    char temp3[256];
    snprintf(temp2, sizeof temp2, "%d%s%d", n, res, gems);
    snprintf(temp3, sizeof temp3, "/home/afridarn/SisOp/modul2/soal1/gacha_gacha/total_gacha_%d/%s_gacha_%d.txt", jumlah, waktu, perfile);
    FILE *out = fopen(temp3, "a");
    fprintf(out, "%s\n", temp2);
    fclose(out);
  }

  while (wait(&mark) > 0)
    ;
}

void gachaWeapons(int jumlah, int perfile, const char *waktu, int n, int gems)
{
  int mark = 0;
  if (fork() == 0)
  {
    FILE *d;
    char buffer[2500];
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    char temp[256];
    snprintf(temp, sizeof temp, "/home/afridarn/SisOp/modul2/soal1/weapons/%s", weaponsDirandom());
    d = fopen(temp, "r");
    fread(buffer, 2500, 1, d);
    fclose(d);

    parsed_json = json_tokener_parse(buffer);

    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    char res[50] = "_weapons_";
    strcat(res, json_object_get_string(name));
    strcat(res, "_");
    strcat(res, json_object_get_string(rarity));
    strcat(res, "_");

    char temp2[256];
    char temp3[256];
    snprintf(temp2, sizeof temp2, "%d%s%d", n, res, gems);
    snprintf(temp3, sizeof temp3, "/home/afridarn/SisOp/modul2/soal1/gacha_gacha/total_gacha_%d/%s_gacha_%d.txt", jumlah, waktu, perfile);
    FILE *out = fopen(temp3, "a");
    fprintf(out, "%s\n", temp2);
    fclose(out);
  }

  while (wait(&mark) > 0)
    ;
}

void createFolder(int jumlah)
{
  int mark = 0;
  if (fork() == 0)
  {
    char temp[256];
    snprintf(temp, sizeof temp, "/home/afridarn/SisOp/modul2/soal1/gacha_gacha/total_gacha_%d", jumlah);
    char *newfolder[] = {"mkdir", temp, NULL};
    execv("/bin/mkdir", newfolder);
  }

  while (wait(&mark) > 0)
    ;
}

void startGacha(int n, int gems)
{
  int jumlah = 0;
  int perfile = 0;
  char sekarang[20];

  while (gems > 160)
  {
    if (n % 2 == 0)
    {
      if (n % 10 == 0 && n % 90 == 0)
      {
        printf("Gacha dimulai..\n");
        jumlah = jumlah + 90;
        createFolder(jumlah);
        time_t currenttime;
        struct tm *hhmmss;
        time(&currenttime);
        hhmmss = localtime(&currenttime);
        sprintf(sekarang, "%d:%d:%d", hhmmss->tm_hour, hhmmss->tm_min, hhmmss->tm_sec);
        perfile = perfile + 10;
      }
      else if (n % 10 == 0)
      {
        time_t currenttime;
        struct tm *hhmmss;
        time(&currenttime);
        hhmmss = localtime(&currenttime);
        sprintf(sekarang, "%d:%d:%d", hhmmss->tm_hour, hhmmss->tm_min, hhmmss->tm_sec);
        perfile += 10;
      }

      gachaWeapons(jumlah, perfile, sekarang, n, gems);
    }
    else if (n % 2 == 1)
    {
      gachaCharacters(jumlah, perfile, sekarang, n, gems);
    }
    n++;
    gems = gems - 160;
    sleep(1);
  }
}

int main()
{
  pid_t pid, sid;

  pid = fork();

  if (pid < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (pid > 0)
  {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0)
  {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/afridarn/SisOp/modul2/soal1/")) < 0)
  {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
  time_t currenttime = time(0);
  int stop = 1;

  while (1)
  {
    currenttime = time(0);

    // epochtime dari 30 Maret 2022 04.44 -> 1648590240
    if (currenttime >= 1648590240 && stop == 1)
    {
      int mark;
      pid_t child_a, child_b;
      int primogems = 79000;

      child_a = fork();

      if (child_a == 0)
      {
        char *download[2][6] = {
            {"wget", "-q", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O", "dbCharacters", NULL},
            {"wget", "-q", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O", "dbItems", NULL}};

        for (int i = 0; i < 2; i++)
        {
          if (fork() == 0)
            continue;
          sleep(5);
          execv("/bin/wget", download[i]);
        }

        char *folder[] = {"mkdir", "gacha_gacha", NULL};
        execv("/bin/mkdir", folder);
      }
      else
      {
        while (wait(&mark) > 0)
          ;
        child_b = fork();
        int mark2;

        if (child_b == 0)
        {
          char *ekstrak[2][4] = {
              {"unzip", "-q", "dbCharacters", NULL},
              {"unzip", "-q", "dbItems", NULL}};

          for (int i = 0; i < 2; i++)
          {
            if (fork() == 0)
              continue;
            sleep(5);
            execv("/bin/unzip", ekstrak[i]);
          }
        }
        else
        {
          while (wait(&mark2) > 0)
            ;
          startGacha(0, primogems);
        }
      }
      stop++;
    }
    else if (currenttime >= 1648601040)
    { // setelah 3 jam
      pid_t child_lagi;
      int mark = 0;
      child_lagi = fork();
      if (child_lagi == 0)
      {
        char *zipHasil[7] = {"zip", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
        execv("/bin/zip", zipHasil);
      }
      else
      {
        while ((wait(&mark)) > 0)
          ;
        char *selesai[4] = {"rm", "-v", "!(not_safe_for_wibu.zip)", NULL};
        execv("/bin/rm", selesai);
        exit(1);
      }
    }

    sleep(30);
  }
}
